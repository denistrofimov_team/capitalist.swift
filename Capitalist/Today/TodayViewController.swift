//
//  TodayViewController.swift
//  Today
//
//  Created by Денис Трофимов on 11.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import NotificationCenter
import PromiseKit

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var budgetName: UILabel!
    @IBOutlet weak var balanceValue: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        balanceValue.textColor = UIColor.whiteColor()
        Manager.setBaseURL("http://capitalist-api.denistrofimov.com");
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        User.current(populate: "primaryBudget").then { (user:User) -> Void in

            let numberFormatter = NSNumberFormatter()
            numberFormatter.numberStyle = .CurrencyStyle
            
            self.budgetName.text = user.primaryBudget!.name
            self.balanceValue.text = numberFormatter.stringFromNumber(user.primaryBudget!.value!)
            
            self.view.invalidateIntrinsicContentSize()
            
            completionHandler(NCUpdateResult.NewData)
            
        }
        
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        
        var insets = UIEdgeInsetsZero
        
        insets.left = defaultMarginInsets.left
        
        return insets
        
    }
    
    @IBAction func plusAction(sender: AnyObject) {
        
        let url = NSURL(string:"capitalistapp://record")
        
        self.extensionContext?.openURL(url!, completionHandler: nil)
        
    }
}
