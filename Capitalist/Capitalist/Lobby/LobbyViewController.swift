//
//  LobbyViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PromiseKit
import Crashlytics
import DigitsKit

class LobbyViewController: UIViewController {
    
    @IBOutlet weak var formView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var user:User?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.activityIndicator.tintColor = ApplicationTintColor
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.activityIndicator.startAnimating()
        self.formView.hidden = true;
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        update()
        
    }
    
    func update(){
    
        self.formView.hidden = true;
        
        User.current(true)
            .always {
                
                self.activityIndicator.stopAnimating()
                
            }
            .then { (user:User) ->Void in
                
                self.user = user
                
                UIApplication.sharedApplication().user = user
                
                // Hotline
                let hotlineUser =  HotlineUser.sharedInstance()
                hotlineUser.email = user.login
                hotlineUser.name = user.name
                hotlineUser.externalID = user.id
                Hotline.sharedInstance().updateUser(hotlineUser)
                
                // Crashlytics
                Crashlytics.sharedInstance().setUserEmail(user.login)
                Crashlytics.sharedInstance().setUserIdentifier(user.id)
                Crashlytics.sharedInstance().setUserName(user.name)
                
                
                UIApplication.sharedApplication().registerForPushNotifications()
                
                self.performSegueWithIdentifier("start", sender: nil)
                
            }
            .error { (error) in
                
                self.formView.hidden = false;
                
        }
        
    }
    
    @IBAction func digitsAuth(sender:UIButton?){
    
        let digits = Digits.sharedInstance()
        
        let configuration = DGTAuthenticationConfiguration(accountFields: .DefaultOptionMask)
        
        let appearance = DGTAppearance()
        
        appearance.accentColor = ApplicationTintColor
        
        appearance.logoImage = UIImage(named: "logotype")
        
        configuration.appearance = appearance
        
        digits.authenticateWithViewController(nil, configuration:configuration) { (session, error) in
            
            if(error != nil) {
                
                return self.alert(error!)
                
            }
            
            Manager.post("authenticator/oauth/digits", parameters: [
                "token":session.authToken,
                "secret":session.authTokenSecret,
                "user_id":session.userID!
                ])
                .then { (response:JSONObject) ->Void in
                    
                    Manager.setAccessToken(response[AccessTokenKey] as! String)
                    
                    self.update()
                    
                }
                .error(self.alert)

            
        }
        
    }
    
    @IBAction func lobbyUnwing(segue:UIStoryboardSegue) {}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "start" {
            
            let app = segue.destinationViewController as! AppViewController
            
            app.user = self.user
            
        }
        
    }
    
}