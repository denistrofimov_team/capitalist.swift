//
//  ForgotViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class ForgotViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var loginIcon: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.hideKeyboardOnTap = true
        
        self.loginIcon.image = self.loginIcon.image?.imageWithRenderingMode(.AlwaysTemplate)
        
    }
    
    @IBAction func remindAction(sender: AnyObject) {
        
        if let login = loginTextField.text {
        
            Manager.post("authenticator/password/forgot", parameters: ["email":login])
                .then { (response:JSONObject) -> Void in

                    self.loginTextField.resignFirstResponder()
                    
                    self.alert("Готово!", message: response["message"] as! String, clouser: {
                        
                        self.navigationController?.popViewControllerAnimated(true);
                        
                    })
                    
                }
                .error(self.alert)
            
        }

    }
}
