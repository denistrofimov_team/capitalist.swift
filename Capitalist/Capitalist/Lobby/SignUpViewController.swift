//
//  SignUpViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginIcon: UIImageView!
    @IBOutlet weak var passwordIcon: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.hideKeyboardOnTap = true
        
        self.loginIcon.image = self.loginIcon.image?.imageWithRenderingMode(.AlwaysTemplate)
        self.passwordIcon.image = self.passwordIcon.image?.imageWithRenderingMode(.AlwaysTemplate)
        
    }
    
    @IBAction func signUpAction(sender: AnyObject) {
        
        Manager.post("authenticator/signup", parameters: ["login":loginTextField.text!, "password":passwordTextField.text!])
            .then { (response:JSONObject) ->Void in
                
                self.passwordTextField.resignFirstResponder()
                self.loginTextField.resignFirstResponder()
                
                //TODO after registration ?
                
            }
            .error(self.alert)
        
    }
    
}
