//
//  SignInViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Alamofire

class SignInViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginIcon: UIImageView!
    @IBOutlet weak var passwordIcon: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.hideKeyboardOnTap = true
        
        self.loginIcon.image = self.loginIcon.image?.imageWithRenderingMode(.AlwaysTemplate)
        self.passwordIcon.image = self.passwordIcon.image?.imageWithRenderingMode(.AlwaysTemplate)
        
    }
    
    @IBAction func signInAction(sender: AnyObject!) {
        
        Manager.post("authenticator/signin", parameters: ["login":loginTextField.text!, "password":passwordTextField.text!])
            .then { (response:JSONObject) ->Void in
                
                Manager.setAccessToken(response[AccessTokenKey] as! String)
                
                self.passwordTextField.resignFirstResponder()
                self.loginTextField.resignFirstResponder()
                
                self.dismiss()
                
            }
            .error(self.alert)
        
    }
    
    func dismiss(){
    
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
        
    }
    
}
