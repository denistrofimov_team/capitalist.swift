//
//  NSURL.swift
//  Capitalist
//
//  Created by Денис Трофимов on 13.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

extension NSURL {
    func queryDictionary() -> [String:AnyObject] {
        let components = self.query?.componentsSeparatedByString("&")
        var dictionary = [String:AnyObject]()
        
        for pairs in components ?? [] {
            let pair = pairs.componentsSeparatedByString("=")
            if pair.count == 2 {
                dictionary[pair[0]] = pair[1]
            } else {
                dictionary[pair[0]] = true
            }
        }
        
        return dictionary
    }
}