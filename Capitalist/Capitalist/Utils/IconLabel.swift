//
//  IconLabel.swift
//  Capitalist
//
//  Created by Трофимов Денис on 07.08.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import MaterialIconsSwift

class IconLabel: UILabel {

    var icon:String? {
    
        didSet {
        
            self.text = MaterialIcons.icon(icon!)
            
        }
        
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.font = MaterialIcons.fontOfSize(24)
        
        self.clipsToBounds = true
        
        self.layer.cornerRadius = self.bounds.width / 2
        
    }

}
