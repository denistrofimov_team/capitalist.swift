//
//  Alert.swift
//  Capitalist
//
//  Created by Денис Трофимов on 30.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import ObjectiveC

var KeyboardGestureRecorgnizerHandler: UInt8 = 0
var hideKeyboardOnTapHandler: UInt8 = 1

// hide keyboard
extension UIViewController {
    
    var _keyboardGestireRecorgnizer:UITapGestureRecognizer! {
        
        get {
            
            return objc_getAssociatedObject(self, &KeyboardGestureRecorgnizerHandler) as! UITapGestureRecognizer
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &KeyboardGestureRecorgnizerHandler, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
        }
        
    }
    
    var hideKeyboardOnTap:Bool! {
        
        get {
            
            var result = objc_getAssociatedObject(self, &hideKeyboardOnTapHandler)
            
            if result == nil {
                
                result = false
                
            }
            
            return result as! Bool
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &hideKeyboardOnTapHandler, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
 
            if newValue == nil || newValue == false{
                
                view.removeGestureRecognizer(self._keyboardGestireRecorgnizer)
                
            } else {
            
                self._keyboardGestireRecorgnizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
                view.addGestureRecognizer(self._keyboardGestireRecorgnizer)
            
            }
            
        }
        
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

// alert
extension UIViewController {

    func alert(error:ErrorType) -> Void {
    
        var message = "Неизвестная ошибка";
        
        switch error {
            
        case BackendError.fatal(let description): message = description
            
        default:break
            
        }
        
        self.alert("Ошибка!", message: message);
        
    }
    
    func alert(error:NSError) -> Void {
        
        self.alert("Ошибка!", message: error.localizedDescription);
        
    }
    
    func alert(title:String, message:String) -> Void {
        
        
        self.alert(title, message: message, clouser: nil);
        
        
    }
    
    func alert(title:String, message:String, clouser:(()->Void)?) -> Void {
        

        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert);
        
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action:UIAlertAction) in
            
            if let function = clouser {
            
                function();
                
            }
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil);
        

    }
    
}

// confirm
extension UIViewController {

    func confirm(title:String, message:String, clouser:(()->Void)?) -> Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "No".localized(), style: .Default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .Default, handler: { (action) in
            
            if let function = clouser {
                
                function()
                
            }
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
}
