//
//  IconView.swift
//  Capitalist
//
//  Created by Трофимов Денис on 18.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class IconView: UIImageView {

    override var image: UIImage? {
    
        get {
        
            return super.image
            
        }
        
        set {
            
            super.image = newValue != nil ? newValue?.tinted : nil
            
        }
        
    }

}
