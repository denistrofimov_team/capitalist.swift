//
//  DateUtils.swift
//  Capitalist
//
//  Created by Денис Трофимов on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

let shortFormat = "yyyy-MM-dd"

class DateUtils {

    static let formatter = NSDateFormatter()
    
    static func parse(string:String, format:String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> NSDate! {
    
        formatter.dateFormat = format
        
        return formatter.dateFromString(string)
    
    }
    
    static func parse(date:NSDate, format:String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> NSString! {
        
        formatter.dateFormat = format
        
        return formatter.stringFromDate(date)
        
    }
    
    static func shortDateString(date:NSDate) -> String! {
        
        formatter.dateFormat = shortFormat
        
        return formatter.stringFromDate(date)
        
    }
    
    static func shortDate(string:String) -> NSDate! {
        
        return self.parse(string, format: shortFormat)
        
    }
    
    static func humanize(date:NSDate) -> String! {
        
        formatter.dateFormat = nil

        formatter.dateStyle = .MediumStyle
        
        return formatter.stringFromDate(date)
        
    }
    
}
