//
//  NumberUtils.swift
//  Capitalist
//
//  Created by Денис Трофимов on 11.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class NumberUtils {

    static let formatter = NSNumberFormatter()
    
    static func numberFromString(string:String) -> Float {
    
        formatter.numberStyle = .DecimalStyle
        
        return formatter.numberFromString(string) as! Float
        
    }
    
    static func stringFromNumber(string:Float) -> String {
        
        formatter.numberStyle = .CurrencyStyle
        
        return formatter.stringFromNumber(string)!
        
    }
    
}
