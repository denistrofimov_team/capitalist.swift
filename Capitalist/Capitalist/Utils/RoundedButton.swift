//
//  RoundedButton.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.backgroundColor = ApplicationTintColor
        self.tintColor = UIColor.whiteColor()
        self.layer.cornerRadius = 28
        self.layer.shadowOffset = CGSizeMake(0, 2);
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.2;
        self.setImage(self.imageForState(.Normal)!.tinted, forState: UIControlState.Normal)
        
    }

}
