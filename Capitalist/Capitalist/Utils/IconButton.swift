//
//  IconButton.swift
//  Capitalist
//
//  Created by Денис Трофимов on 20.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import MaterialIconsSwift

class IconButton: UIButton {

    var icon:String? {
        
        didSet {
            
            self.setTitle(MaterialIcons.icon(icon!), forState: .Normal)
            
        }
        
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.titleLabel?.font = MaterialIcons.fontOfSize(24)
        
    }
    
}
