//
//  UIImage.swift
//  Capitalist
//
//  Created by Трофимов Денис on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

extension UIImage {

    var tinted: UIImage {
    
        return self.imageWithRenderingMode(.AlwaysTemplate)
        
    }
    
    class func empty() -> UIImage{
    
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(36, 36), false, 0.0)
        
        let empty = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return empty
        
    }
    
}
