//
//  UITableViewCell.swift
//  Capitalist
//
//  Created by Трофимов Денис on 14.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import ObjectiveC


var budgetValueHandle: UInt8 = 0

extension UITableViewCell {

    var badgeValue:String? {
    
        get {
            
            return objc_getAssociatedObject(self, &budgetValueHandle) as? String
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &budgetValueHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            if newValue != nil {
            
                var badge:UILabel;
                
                if self.accessoryView is UILabel {
                
                    badge = self.accessoryView as! UILabel
                    
                } else {
                
                    badge = UILabel(frame:CGRectMake(0,0, 0, 30))
                    badge.backgroundColor = UIColor.clearColor()
                    badge.textColor = UIColor.whiteColor()
                    badge.font = UIFont .systemFontOfSize(12.0)
                    self.accessoryView = badge
                    
                }
                
                badge.text = newValue
                badge.sizeToFit()
                
            } else {
            
                self.accessoryView = nil
                
            }
            
        }
        
    }
    
}
