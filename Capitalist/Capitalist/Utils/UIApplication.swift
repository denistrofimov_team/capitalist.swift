//
//  UIApplication.swift
//  Capitalist
//
//  Created by Денис Трофимов on 07.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Branch
import ObjectiveC

extension UIApplication {
    
    func registerForPushNotifications() {
        
        self.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
        self.registerForRemoteNotifications()
        
    }
    
}

extension UIApplication {
    
    func share(inController:UIViewController) {
        
        // Initialize a Branch Universal Object for the page the user is viewing
        let branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "capitalist.app")
        branchUniversalObject.title = NSBundle.mainBundle().infoDictionary![kCFBundleNameKey as String] as! String
        branchUniversalObject.contentDescription = "capitalist.description".localized()
        
        let linkProperties = BranchLinkProperties()
        
        linkProperties.feature = "sharing"
        
        branchUniversalObject.showShareSheetWithLinkProperties(linkProperties, andShareText: "capitalist.share-text".localized(),
                                                               fromViewController: inController,
                                                               completion: nil)
        
    }
    
}

var ActiveBudgetHandle: UInt8 = 0
let ActiveBudgetChangeNotification:String = "ActiveBudgetChangeNotification"

var CurrentUserHandle: UInt8 = 1

var CreatedBudgetHandle: UInt8 = 2

extension UIApplication {
    
    var activeBudget:Budget! {
        
        get {
            
            let budget = objc_getAssociatedObject(self, &ActiveBudgetHandle)
            
            if budget == nil {
             
                return nil
                
            }
            
            return budget as! Budget
        
        }
        
        set {
            
            objc_setAssociatedObject(self, &ActiveBudgetHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            NSNotificationCenter.defaultCenter().postNotificationName(ActiveBudgetChangeNotification, object: newValue)
            
        }
        
    }
    
    var user:User! {
        
        get {
            
            return objc_getAssociatedObject(self, &CurrentUserHandle) as! User
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &CurrentUserHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            if newValue != nil {
            
                if self.createdBudget != nil {
                
                    let budget = newValue.budgets[createdBudget]
                    
                    if budget != nil {
                        
                        self.activeBudget = budget
                        
                    }
                    
                } else if newValue.primaryBudget != nil {
                
                    
                    let budget = newValue.budgets[newValue.primaryBudget!.id!]
                    
                    if budget != nil {
                        
                        self.activeBudget = budget
                        
                    }
                    
                }
                
            } else {
            
                self.activeBudget = nil
                
            }
            
        }
        
    }
    
    var createdBudget:Identifier! {
        
        get {
            
            let value = objc_getAssociatedObject(self, &CreatedBudgetHandle)
            
            if value == nil {
                
                return nil
                
            }
            
            return value as! Identifier
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &CreatedBudgetHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
        }
        
    }
    
}