//
//  String.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

extension String {

    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}
