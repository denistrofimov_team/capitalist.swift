//
//  Moment.swift
//  Capitalist
//
//  Created by Трофимов Денис on 24.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import SwiftMoment

extension Moment {

    var standaloneMonthName:String {
    
        let formatter = NSDateFormatter()
    
        formatter.locale = locale
        
        return formatter.standaloneMonthSymbols[month - 1]
        
    }
    
}
