//
//  CalendarViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 09.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import JTCalendar

class CalendarViewController: UIViewController, JTCalendarDelegate {

    @IBOutlet weak var calendarContainer: UIView!
    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
    @IBOutlet weak var calendarContentView: JTHorizontalCalendarView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!

    var date:NSDate? {
    
        didSet {
        
            if self.calendarManager != nil {
            
                self.calendarManager?.setDate(date)
            
            }
            
        }
    
    }
    
    var calendarManager:JTCalendarManager?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.calendarContainer.layer.cornerRadius = 7
        self.calendarContainer.layer.shadowOffset = CGSizeMake(0, 2);
        self.calendarContainer.layer.shadowRadius = 3;
        self.calendarContainer.layer.shadowOpacity = 0.2;
        
        self.calendarManager = JTCalendarManager()
        self.calendarManager?.delegate = self
        self.calendarManager?.menuView = calendarMenuView
        self.calendarManager?.contentView = calendarContentView
        self.calendarManager?.setDate(self.date ?? NSDate())
        
    }
    
    // MARK: - Calendar
    
    func calendar(calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        
        let dayView = dayView as! JTCalendarDayView
        
        let color = UIApplication.sharedApplication().keyWindow?.tintColor
        
        dayView.hidden = false;
        
        // Test if the dayView is from another month than the page
        // Use only in month mode for indicate the day of the previous or next month
        if dayView.isFromAnotherMonth {
            dayView.hidden = false
            dayView.textLabel.textColor = UIColor.lightGrayColor()
        }
            
            // Selected date
        else if self.date != nil && self.calendarManager!.dateHelper.date(self.date, isTheSameDayThan: dayView.date) {
            dayView.circleView.hidden = false
            dayView.circleView.backgroundColor = color
            dayView.dotView.backgroundColor = UIColor.whiteColor()
            dayView.textLabel.textColor = UIColor.whiteColor()
        }
            
            // Today
        else if self.calendarManager!.dateHelper.date(NSDate(), isTheSameDayThan: dayView.date) {
            dayView.circleView.hidden = false
            dayView.circleView.backgroundColor = color!.colorWithAlphaComponent(0.1)
            dayView.dotView.backgroundColor = UIColor.whiteColor()
            dayView.textLabel.textColor = UIColor.blackColor()
        }
       
            // Another day of the current month
        else{
            dayView.circleView.hidden = true
            dayView.dotView.backgroundColor = UIColor.redColor()
            dayView.textLabel.textColor = UIColor.blackColor()
        }
        
    }

    func calendar(calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        
        let dayView = dayView as! JTCalendarDayView
        
        // Use to indicate the selected date
        self.date = dayView.date;
        
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        
        UIView.transitionWithView(dayView, duration: 0.25, options: [], animations: { 
            dayView.circleView.transform = CGAffineTransformIdentity;
            self.calendarManager?.reload()
            }, completion: nil)
        
    }
    
}
