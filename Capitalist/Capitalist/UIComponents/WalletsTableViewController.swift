//
//  UsersTableViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 10.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class WalletsTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var wallets:[Wallet]?
    var wallet:Wallet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.emptyDataSetSource = self
        
        self.tableView.emptyDataSetDelegate = self
        
        self.title = "Wallet".localized()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.wallets = UIApplication.sharedApplication().activeBudget.wallets
        
        self.tableView.reloadData()
        
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.wallets!.count
    
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        let wallet = self.wallets![indexPath.row]
        
        cell.textLabel?.text = wallet.name
        
        cell.detailTextLabel?.text = wallet.user?.displayName

        return cell
    }
 

    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "wallets" {
            
            if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPathForCell(cell) {
                
                self.wallet = self.wallets![indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - Empty Dataset -
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No wallets".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no wallets in your budget".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Add wallet".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        return self.wallets!.count == 0
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        self.performSegueWithIdentifier("add_wallet", sender: scrollView)
        
    }

}
