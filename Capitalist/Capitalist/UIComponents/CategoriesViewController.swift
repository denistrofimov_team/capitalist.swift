//
//  CategoriesViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 09.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class CategoriesViewController: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    var category:Category? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.emptyDataSetDelegate = self
        
        self.tableView.emptyDataSetSource = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return UIApplication.sharedApplication().activeBudget.categories.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("category")  as! CategoryTableViewCell
        
        let category = UIApplication.sharedApplication().activeBudget.categories[indexPath.row]
        
        cell.nameLabel!.text = category.name
        
        cell.iconLabel?.icon = category.icon
        
        cell.iconLabel?.backgroundColor = category.color
        
        cell.iconLabel?.textColor = category.iconColor
        
        return cell
        
    }
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "categories" {
            
            if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPathForCell(cell) {
                
                self.category = UIApplication.sharedApplication().activeBudget.categories[indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - Empty Dataset -
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No categories".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no categories in your budget".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Add category".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        return UIApplication.sharedApplication().activeBudget.categories.count == 0
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        self.performSegueWithIdentifier("add_category", sender: scrollView)
        
    }
    
}
