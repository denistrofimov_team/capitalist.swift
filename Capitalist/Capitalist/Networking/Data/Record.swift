//
//  Record.swift
//  Capitalist
//
//  Created by Трофимов Денис on 07.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit

typealias Records = [Record]

class Record: Model {

    static let resource:Resource<Record> = Resource<Record>(name: "record")

    var userIdentifier: Identifier!
    var categoryIdentifier: Identifier!
    var walletIdentifier: Identifier!
    
    var date: NSDate!

    var value: Float!
    
    var user: User!
    
    var category: Category!
    
    var wallet: Wallet!
    
    var budget: Budget!

    var tags: [String] = []
    
    override func parse(json: JSONObject) {
        
        super.parse(json)
        
        self.categoryIdentifier = json["category"] as! Identifier
        
        if json["wallet"] is Identifier {
        
            self.walletIdentifier = json["wallet"] as! Identifier
            
        }
        
        if json["user"] is JSONObject {
        
            let user = User()
            
            user.parse(json["user"] as! JSONObject)
            
            self.user = user
            
        } else {
        
            self.userIdentifier = json["user"] as! Identifier
            
        }
        
        self.tags = json["tags"] as! [String]
        
        self.value = json["value"] as! Float
        
        self.date = DateUtils.parse(json["date"] as! String)
        
    }
    
    override func json() -> JSONObject {
        
        var data = super.json()
        
        data["value"] = self.value
        data["category"] = self.category.id
        data["wallet"] = self.wallet.id
        data["budget"] = self.budget.id
        data["tags"] = self.tags
        data["user"] = self.user.id
        data["date"] = DateUtils.parse(self.date ?? NSDate())
        
        return data
        
    }
    
    override func valid() -> Bool {
        
        if super.valid() == false {
        
            return false
            
        }
        
        if self.budget == nil {
        
            return false
        
        }
        
        if self.category == nil {
            
            return false
            
        }
        
        if self.wallet == nil {
            
            return false
            
        }
        
        if self.user == nil {
            
            return false
            
        }
        
        if self.value == nil {
            
            return false
            
        }
        
        return true
        
    }
    

    func save() -> Promise<Record> {
        
        return Record.resource.save(self.json()).then({ (record) -> Record in
    
            let budget = self.budget
            
            record.wallet = budget.wallets[record.walletIdentifier]
            
            record.category = budget.categories[record.categoryIdentifier]
            
            if record.user == nil {
            
                record.user = budget.users[record.userIdentifier]
                
            }
            
            record.budget = budget
            
            self.parse(record.json())
            
            return record
            
        })
        
    }
    
    func delete() -> Promise<Void> {
        
        return Record.resource.delete(self.json())
        
    }
    
}
