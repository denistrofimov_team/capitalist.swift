//
//  Model.swift
//  Capitalist
//
//  Created by Денис Трофимов on 29.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit
import Alamofire

public typealias Identifier = String

public typealias Identifiers = [Identifier]

public protocol IdentifierConvertible {
    
    var id: Identifier? { get }
    
}

public func ==(lhs: Model, rhs: Model) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

public class Model : Hashable  {
    
    var _id:String?
    
    public var hashValue: Int {
    
        if _id == nil {
        
            return 0
            
        }
        
        return (_id?.hashValue)!
        
    }
    

    
    func parse(json:JSONObject){
        
        self.id = json["_id"] as? Identifier
        
    }
    
    init(id:Identifier){
        
        self.id = id
        
    }
    
    required public init(){
    
        self.id = nil;
        
    }
    
    func invalid() -> String? {
        
        return nil
        
    }
    
    func valid() -> Bool {
        
        return self.invalid() == nil
    
    }
    
    func json() -> JSONObject {
        
        if _id == nil {
        
            return [:]
            
        }
        
        return ["_id":self._id!]
        
    }
    
}

extension String : IdentifierConvertible {

    public var id: Identifier? {
        return self
    }
    
}

extension Model : IdentifierConvertible {

    public var id: Identifier? {
        
        get {
            
            if _id == nil {
            
                return nil
                
            }
            
            return _id!
            
        }
        
        set {
            
            _id = newValue
            
        }
        
    }
    
}

extension Dictionary : IdentifierConvertible {

    
    public var id: Identifier? {
        
        get {
            
            return self["_id" as! Key] as? Identifier
            
        }
        
    }

    
}
