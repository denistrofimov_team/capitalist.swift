//
//  Category.swift
//  Capitalist
//
//  Created by Денис Трофимов on 06.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import PromiseKit

typealias Categories = [Category]

class Category: Model {
    
    static let resource:Resource<Category> = Resource<Category>(name: "category")
    
    var name:String?
    var color:UIColor? = UIColor.whiteColor()
    var iconColor:UIColor? = ApplicationTintColor
    var icon:String?
    var budget:Budget?
    var records:[Record] = []
    
    override func parse(json: JSONObject) {
        
        super.parse(json)
        
        self.name = json["name"] as? String
        
        self.color = UIColor(rgba: json["color"] as! String)
        
        self.iconColor = UIColor(rgba: json["icon_color"] as! String)
        
        self.icon = json["icon"] as? String
        
        let records = json["records"]
        
        if(records is JSONArray) {
            
            self.records = []
            
            for _record in records as! [JSONObject] {
                
                let record = Record()
                record.parse(_record)
                self.records.append(record)
                
            }
            
        }
        
    }
    
    override func json() -> JSONObject {
        
        var data = super.json()
        
        data["name"] = self.name!
        data["color"] = self.color?.hexString(false)
        data["icon_color"] = self.iconColor?.hexString(false)
        data["icon"] = self.icon
        
        if self.budget != nil {
        
            data["budget"] = self.budget!.id
            
        }
        
        return data
        
    }
    
    override func valid() -> Bool {
        
        if super.valid() == false {
            
            return false
            
        }
        
        if self.budget == nil {
            
            return false
            
        }
        
        if self.icon == nil {
            
            return false
            
        }
        
        if self.color == nil {
            
            return false
            
        }
        
        if self.iconColor == nil {
            
            return false
            
        }
        
        if self.name == nil {
            
            return false
            
        }
        
        return true
        
    }
    
    override func invalid() -> String? {
        
        if let string = super.invalid() {
            
            return string
            
        }
        
        if self.budget == nil {
            
            return "budget"
            
        }
        
        if self.icon == nil {
            
            return "icon"
            
        }
        
        if self.color == nil {
            
            return "color"
            
        }
        
        if self.iconColor == nil {
            
            return "iconColor"
            
        }
        
        if self.name == nil {
            
            return "name"
            
        }
        
        return nil
        
    }
    
    
    func save() -> Promise<Category> {
        
        return Category.resource.save(self.json()).then({ (category) -> Category in

            self.parse(category.json())
            
            return category
            
        })
        
    }
    
    func delete() -> Promise<Void> {
        
        return Category.resource.delete(self.json())
        
    }
    
    
}

