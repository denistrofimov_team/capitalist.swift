//
//  Wallet.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit

class Wallet: Model {

    static let resource:Resource<Wallet> = Resource<Wallet>(name: "wallet")
    
    var name:String?
    
    var budget:Budget?
    
    var user:User?
    
    override func parse(json: JSONObject) {
        
        super.parse(json)

        self.name = json["name"] as? String
        
        if self.budget != nil {
            
            let user = json["user"] as! Identifier
            
            self.user = self.budget?.users[user]
            
        }
        
    }
    
    override func valid() -> Bool {
        
        if super.valid() == false {
            
            return false
            
        }
        
        if self.name == nil {
            
            return false
            
        }
        
        if self.user == nil {
            
            return false
            
        }
        
        return true
        
    }
    
    override func json() -> JSONObject {
        
        var data = super.json()
        
        data["name"] = self.name
        
        if self.budget != nil {
            
            data["budget"] = self.budget!.id
            
        }
        
        if self.user != nil {
            
            data["user"] = self.user!.id
            
        }
        
        return data
        
    }
    
    func save() -> Promise<Wallet> {
        
        print("\(self.json())")
        
        return Wallet.resource.save(self.json()).then({ (wallet) -> Wallet in
            
            self.id = wallet.id
            
            return self
            
        })
        
    }
    
    func delete() -> Promise<Void> {
        
        return Wallet.resource.delete(self.json())
        
    }
    
}
