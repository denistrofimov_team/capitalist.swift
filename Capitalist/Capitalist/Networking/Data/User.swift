//
//  User.swift
//  Capitalist
//
//  Created by Денис Трофимов on 30.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit

typealias Users = [User]

class User: Model {
    
    static let resource:Resource<User> = Resource<User>(name: "user")
    
    var login:String?
    var name:String?
    var digitsID:String?
    var avatar:NSURL?
    var primaryBudgetIdentifier:Identifier?
    var primaryBudget:Budget?
    
    var displayName:String {
    
        return name ?? login ?? id!
        
    }
    
    var budgets:[Budget] = [];
    
    static private var _currentUser:User!;
    
    override func parse(json:JSONObject){
        
        super.parse(json)
        
        self.login = json["login"] as? String
        
        self.name = json["name"] as? String
        
        if json["digitsId"] is String {
        
            self.digitsID = json["digitsId"] as? String
        
        }
        
        if json["avatar"] is String {
            
            self.avatar = Manager.publicResource(NSURL(string: (json["avatar"] as? String)!)!)
            
        }
        
        let budgets = json["budgets"]
        
        if(budgets is Array<JSONObject>) {
            
            self.budgets = []
            
            for _budget in budgets as! [JSONObject] {
                
                let budget = Budget()
                budget.parse(_budget)
                self.budgets.append(budget)
                
            }
            
        }
        
        if let primaryBudget = json["primaryBudget"] where primaryBudget is JSONObject {
        
            self.primaryBudget = Budget()
            
            self.primaryBudget?.parse(primaryBudget as! JSONObject)
            
        }
        
        if let primaryBudget = json["primaryBudget"] where primaryBudget is String {
            
            self.primaryBudgetIdentifier = json["primaryBudget"] as? String
            
            self.primaryBudget = self.budgets[self.primaryBudgetIdentifier!]
            
        }

    }
    
    // MARK: - Current user
    
    static func current(forse:Bool = false, populate:String = "budgets.categories budgets.users budgets.wallets primaryBudget") -> Promise<User> {
        
        if !Manager.isAuthenticated() {
            
            return Promise(error:BackendError.fatal("Пользователь не авторизован"))
            
        }
        
        if _currentUser != nil && !forse {
            
            return Promise(_currentUser);
            
        }
        
        return resource.perform("current", parameters: ["populate":populate])
            
            .then { (response:JSONObject) -> User in
                
                _currentUser = User()
                _currentUser.parse(response)
                
                return _currentUser;
                
        }
        
    }
    
    func registerDeviceToken(deviceToken:String) {
    
        User.resource.perform("register_device_token", parameters: ["device_token":deviceToken, "platform":"ios"])
        
    }
    
    override func json() -> JSONObject {
        
        var json = super.json()
        
        if self.name != nil {
            
            json["name"] = self.name
            
        }

        if self.avatar != nil {
            
            json["avatar"] = Manager.relativePublicResource(self.avatar!)
            
        }
        
        if self.primaryBudget != nil {
        
            json["primaryBudget"] = self.primaryBudget?.id
            
        }
        
        if self.digitsID != nil {
            
            json["digitsId"] = self.digitsID
            
        }
        
        return json
        
    }
    
    func save() -> Promise<User> {
        
        print("\(self.json())")
        
        return User.resource.save(self.json()).then({ (user) -> User in

            return self
            
        })
        
    }
    
    static func uploadAvatar(image:UIImage) -> Promise<NSURL> {
        
        return Manager.upload("api/upload/image", image: image).then { (response) -> NSURL in
            
            return Manager.publicResource(NSURL(string: response["file"] as! String)!)

        }
        
    }
    
    static func contacts(ids:[String], except:[String] = []) -> Promise<Users> {
        
        return Manager.post("api/user/contacts", parameters: ["contacts":ids, "except":except]).then({ (response) -> Users in
            
            let sources = response["data"] as! JSONArray
            
            var users:Users = []
            
            for source in sources {
            
                let user = User()
                
                user.parse(source)
                
                users.append(user)
                
            }
            
            return users
            
        })
        
    }
    
}