//
//  Budget.swift
//  Capitalist
//
//  Created by Денис Трофимов on 05.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit
import SwiftyJSON

class Budget: Model {
    
    static let resource:Resource<Budget> = Resource<Budget>(name: "budget")
    
    var name:String?
    var value:Float?
    var categories:[Category] = []
    var wallets:[Wallet] = []
    var users:[User] = []
    var owner:User?
    
    override func parse(json: JSONObject) {
        
        super.parse(json)
        
        self.name = json["name"] as? String
        
        self.value = json["value"] as? Float
        
        let owner = json["owner"]
        
        self.owner = User()
        
        if owner is JSONObject {
            
            self.owner?.parse(owner as! JSONObject)
            
        } else {
        
            self.owner!.id = owner as? String
            
        }
        
        let categories = json["categories"]
        
        if(categories is [JSONObject]) {
            
            self.categories = []
            
            for _category in categories as! [JSONObject] {
                
                let category = Category()
                category.parse(_category)
                
                category.budget = self
                
                self.categories.append(category)
                
            }
            
        }
        
        let users = json["users"]
        
        if(users is [JSONObject]) {
            
            self.users = []
            
            for _user in users as! [JSONObject] {
                
                let user = User()
                user.parse(_user)
                self.users.append(user)
                
            }
            
        }
        
        let wallets = json["wallets"]
        
        if(wallets is [JSONObject]) {
            
            self.wallets = []
            
            for _wallet in wallets as! [JSONObject] {
                
                let wallet = Wallet()
                
                wallet.budget = self
                
                wallet.parse(_wallet)
                
                self.wallets.append(wallet)
                
            }
            
        }
        
    }
    
    override func valid() -> Bool {
        
        if super.valid() == false {
            
            return false
            
        }
        
        if self.name == nil {
            
            return false
            
        }
        
        return true
        
    }
    
    override func json() -> JSONObject {
        
        var json = super.json()
        
        json["name"] = self.name
        
        if self.owner != nil {
            
            json["owner"] = self.owner?.id
            
        }
        
        json["users"] = self.users.map({ (user) -> String in
            
            return user.id!
            
        })
        
        return json
        
    }
    
    func save() -> Promise<Budget> {
        
        return Budget.resource.save(self.json()).then({ (budget) -> Budget in
            
            self.id = budget.id
            
            return self
            
        })
        
    }
    
    func delete() -> Promise<Void> {
        
        return Budget.resource.delete(self.json())
        
    }
    
    func removeUser(user:User) -> Promise<Void> {
        
        if self.owner!.id == user.id {
            
            return Promise(error: BackendError.fatal("Нельзя удалить владельца бюджета"))
            
        }
        
        var json = self.json()
        
        var users = json["users"] as! [String]
        
        users.removeAtIndex(users.indexOf({ (_user) -> Bool in
            
            return user.id == _user.id
            
        })!)
        
        json["users"] = users
        
        return Budget.resource.save(json).then({ (budget) -> Void in
            
            return self
            
        })
        
    }
    
    func share(email:String? = nil, users:Identifiers? = nil) -> Promise<String> {
        
        let json = self.json()
        
        guard let email = email else {
        
            guard let users = users else { return Promise(error:BackendError.fatal("No params"))}
            
            return Budget.resource.perform(json, name:"share", parameters: ["users":users]).then { (response:JSONObject) -> String in
                
                return response["message"] as! String
                
            }
            
        }
        
        return Budget.resource.perform(json, name:"share", parameters: ["email":email]).then { (response:JSONObject) -> String in
            
            return response["message"] as! String
            
        }

    }
    
    func getRecords() -> Promise<CollectionResult<Record>>{
        
        return Record.resource.getAll(query:JSON(["budget":id!]).rawString()).then { (collection) -> CollectionResult<Record> in
            
            for record in collection.data {
                
                record.category = self.categories[record.categoryIdentifier]
                
                if record.walletIdentifier != nil {
                
                    record.wallet = self.wallets[record.walletIdentifier]
                    
                }
                
                record.user = self.users[record.userIdentifier]
                
                record.budget = self
                
            }
            
            return collection
            
        }
        
    }
    
    func getCategories(populate:String = "records") -> Promise<CollectionResult<Category>>{
        
        return Category.resource.getAll(populate:populate, query:JSON(["budget":id!]).rawString()).then { (collection) -> CollectionResult<Category> in
            
            return collection
            
        }
        
    }
    
}
