//
//  CapitalistAPI.swift
//  Capitalist
//
//  Created by Денис Трофимов on 28.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Alamofire
import PromiseKit
import SwiftyJSON

let AccessTokenKey = "access_token"
let AccessTokenHeader = "X-Access-Token"
let CapitalistGroupIdentifier = "group.com.denistrofimov.Capitalist"
let PublicResourcesRoot = "http://capitalist.denistrofimov.com/"

enum BackendError: ErrorType {
    case fatal(String)
}

typealias JSONObject = [String:AnyObject]
typealias JSONArray = [JSONObject]
typealias Parameters = Dictionary<String, AnyObject!>?

class Manager: AnyObject {
    
    //MARK: - var
    
    static private var _baseURL:URLStringConvertible!;
    static private var _accessToken:String!;
    
    //MARK: - Utils
    
    static func defaultHeaders() -> [String : String]{
    
        if (_accessToken != nil) {
            return [AccessTokenHeader:_accessToken]
        }
        
        return [:]
        
    }
    
    static func setBaseURL(baseURL:URLStringConvertible) -> Manager.Type{
    
        _baseURL = baseURL
        
        let defaults = NSUserDefaults(suiteName: CapitalistGroupIdentifier)!
        
        let accessToken = defaults.objectForKey(AccessTokenKey);
        
        if(accessToken != nil){
        
            _accessToken = accessToken as! String;
            
        }
        
        return self;
        
    }
    
    static func setAccessToken(accessToken:String) -> Manager.Type{
        
        _accessToken = accessToken;
        
        let defaults = NSUserDefaults(suiteName: CapitalistGroupIdentifier)!
        defaults.setObject(_accessToken, forKey: AccessTokenKey)
        
        return self;
        
    }

    static func getAccessToken() -> String {
        
        return _accessToken
        
    }
    
    static func clearAccessToken() -> Manager.Type{
        
        _accessToken = nil;
        
        let defaults = NSUserDefaults(suiteName: CapitalistGroupIdentifier)!
        defaults.removeObjectForKey(AccessTokenKey)
        
        return self;
        
    }
    
    static func isAuthenticated() -> Bool {
    
        return _accessToken != nil;
    
    }
    
    static func url(input:URLStringConvertible) -> URLStringConvertible{

        return "\(_baseURL)/\(input)";
    
    }
    
    // MARK: - Methods
    
    static func post(url:URLStringConvertible, parameters:Parameters = nil) -> Promise<JSONObject>{
    
        return Promise { fulfill, reject in
 
            let request = Alamofire.request(.POST, self.url(url), parameters: parameters, encoding: .JSON, headers: self.defaultHeaders())
                .responseJSON(completionHandler: { (response) in
                    
                    switch response.result {
                    case .Success(let data):
                        
                        let res:JSONObject = data as! JSONObject
                        
                        let error = checkForError(res)
                        
                        if (error == nil){
                            
                            fulfill(data as! JSONObject);
                            
                        } else {
                            
                            reject(error)
                            
                        }
                        
                    case .Failure(let error):
                        reject(error)
                    }
                    
                })
            
            print(request);

        };
    }
    
    static func put(url:URLStringConvertible, parameters:Parameters = nil)  -> Promise<JSONObject> {
        
        return Promise { fulfill, reject in
            
            let request = Alamofire.request(.PUT, self.url(url), parameters: parameters, encoding: .JSON, headers: self.defaultHeaders())
                .responseJSON(completionHandler: { (response) in
                    
                    switch response.result {
                    case .Success(let data):
                        
                        let res:JSONObject = data as! JSONObject
                        
                        let error = checkForError(res)
                        
                        if (error == nil){
                            
                            fulfill(data as! JSONObject);
                            
                        } else {
                            
                            reject(error)
                            
                        }
                        
                    case .Failure(let error):
                        reject(error)
                    }
                    
                })
            
             print(request);
            
        };
        
    }
    
    static func delete(url:URLStringConvertible, parameters:Parameters = nil)  -> Promise<JSONObject> {
        
        return Promise { fulfill, reject in
            
            let request = Alamofire.request(.DELETE, self.url(url), parameters: parameters, encoding: .JSON, headers: self.defaultHeaders())
                
                .responseJSON(completionHandler: { (response) in
                    
                    switch response.result {
                    case .Success(let data):
                        
                        let res:JSONObject = data as! JSONObject
                        
                        let error = checkForError(res)
                        
                        if (error == nil){
                            
                            fulfill(data as! JSONObject);
                            
                        } else {
                            
                            reject(error)
                            
                        }
                        
                    case .Failure(let error):
                        reject(error)
                    }
                    
                })
            
            print(request);
            
        }
        
    }
    
    static func get(url:URLStringConvertible, parameters:Parameters = nil)  -> Promise<JSONObject> {

        return Promise { fulfill, reject in
            
            let request = Alamofire.request(.GET, self.url(url), parameters: parameters, encoding: .URL, headers: self.defaultHeaders())
                .responseJSON(completionHandler: { (response:Response<AnyObject,NSError>) in
                    
                    switch response.result {
                    case .Success(let data):
                        
                        let res:JSONObject = data as! JSONObject
                        
                        let error = checkForError(res)
                        
                        if (error == nil){
                        
                            fulfill(data as! JSONObject);
                            
                        } else {
                        
                            reject(error)
                        
                        }
                        
                        
                    case .Failure(let error):
                        reject(error)
                    }
                    
                })
            
            print(request);
            
            }
        
    }
    
    static func upload(url:URLStringConvertible, image:UIImage)  -> Promise<JSONObject> {
        
        return Promise { fulfill, reject in
            
            Alamofire.upload(
                .POST,
                self.url(url),
                headers:self.defaultHeaders(),
                multipartFormData: { (formData) in
                
                    let data = UIImageJPEGRepresentation(image, 0.5)
                    
                    if data != nil {
                    
                        formData.appendBodyPart(data: data! , name: "file", fileName: "avatar.jpg", mimeType: "image/jpeg")
                        
                    }

                },
                encodingCompletion: { (encodingResult) in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        
                        print("\(upload)")
                        
                        upload.responseJSON(completionHandler: { (response:Response<AnyObject,NSError>) in
                            
                            switch response.result {
                            case .Success(let data):
                                
                                let res:JSONObject = data as! JSONObject
                                
                                let error = checkForError(res)
                                
                                if (error == nil){
                                    
                                    fulfill(data as! JSONObject);
                                    
                                } else {
                                    
                                    reject(error)
                                    
                                }
                                
                                
                            case .Failure(let error):
                                reject(error)
                            }
                            
                        })
                    case .Failure(let encodingError):
                        reject(encodingError)
                    }
                })
            }
        
    }
    
    static func checkForError(response:AnyObject) -> BackendError! {
        
        let data:JSONObject = response as! JSONObject
        
        if(data["code"] != nil && data["message"] != nil) {
        
           return  BackendError.fatal(response["message"] as! String)
            
        }

        return nil
        
    }
    
    static func publicResource(url:NSURL) -> NSURL{
    
        return NSURL(string:"\(PublicResourcesRoot)\(url.absoluteString)")!
        
    }
    
    static func relativePublicResource(url:NSURL) -> String{
        
        return url.absoluteString.stringByReplacingOccurrencesOfString(PublicResourcesRoot, withString: "")
        
    }
    
}
