//
//  Resource.swift
//  Capitalist
//
//  Created by Денис Трофимов on 05.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit

struct CollectionResult<Type:Model> {

    let total:Int
    let page:Int
    let pageSize:Int
    let data:[Type]
    
}

class Resource<Type:Model> {

    let name:String
    let url:String
    
    init(name:String){
    
        self.name = name
        self.url = "api/\(name)/:_id"
        
    }
    
    func getAll(page:Int = 1, per_page:Int = 1000, populate:String! = nil, query:String! = nil) -> Promise<CollectionResult<Type>>{
    
        var params:Parameters = ["page":page, "per_page": per_page]
        
        if query != nil {
        
            params!["query"] = query
            
        }
        
        if populate != nil {
        
            params!["populate"] = populate
        
        }
        
        return Manager.get(self.buildUrl(self.url, parameters: nil), parameters: params).then { (response) -> Promise<CollectionResult<Type>> in
            
            var result:[Type] = [];
            let responseArray:JSONArray = response["result"] as! JSONArray
            let meta = response["meta"] as! JSONObject
            let total = meta["count"] as! Int
            let page = meta["page"] as! Int
            let size = meta["page"] as! Int
            
            for object in responseArray {
            
                let model = Type()
                model.parse(object)
                
                result.append(model)
                
            }
            
            return Promise(CollectionResult(total: total, page: page, pageSize: size, data: result))
            
        }
    
    }
    
    func perform(name:String, parameters:Parameters) -> Promise<JSONObject> {
        
        return Manager.post("api/\(self.name)/\(name)", parameters: parameters);
        
    }
    
    func perform(data:Parameters, name:String, parameters:Parameters) -> Promise<JSONObject> {
        
        print("api/\(self.name)/\(data!.id!)/\(name)")
        
        return Manager.post("api/\(self.name)/\(data!.id!)/\(name)", parameters: parameters);
        
    }
    
    func save(model:Parameters) -> Promise<Type> {
        
        return self._save(model).then { (response) -> Promise<Type> in
            
            let model = Type()
            
            model.parse(response)
            
            return Promise(model)
            
        }
    
    }
    
    func _save(model:Parameters) -> Promise<JSONObject> {
        
        return (model!.id != nil ? Manager.put(buildUrl(url, parameters: model), parameters: model) : Manager.post(buildUrl(url, parameters: model), parameters: model))
    }
    
    func delete(model:Parameters) -> Promise<Void> {
        
        if model!.id == nil {
        
            return Promise(error:BackendError.fatal("Identifier is undefined"))
            
        }
        
        return Manager.delete(buildUrl(url, parameters: model), parameters: nil).asVoid()
    
    }
    
    /**
     Build actual URL from url string and parameters
     */
    func buildUrl(string:String, parameters:Parameters) -> String{
    
        let components:[String] = string.componentsSeparatedByString("/")
        
        var resultUrl:[String] = []
        
        for component in components {
            
            if(component.hasPrefix(":")){
                
                if parameters == nil {
                
                    resultUrl.append("" )
                
                } else {
                
                    let name = component.stringByReplacingOccurrencesOfString(":", withString:"")
                    
                    let value = parameters![name]
                    
                    resultUrl.append(value != nil ? "\(value!)" : "" )
                    
                }
                
            } else {
                
                resultUrl.append(component)
                
            }
            
        }
        
        return resultUrl.joinWithSeparator("/");
    
    }
    
}
