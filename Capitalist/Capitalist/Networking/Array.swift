//
//  Collection.swift
//  Capitalist
//
//  Created by Денис Трофимов on 05.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

extension Array where Element: Model {

    subscript(id:String) -> Element! {
        
        let variants = self.filter { (element) -> Bool in
            
            return element.id == id;
        }
        
        if variants.count == 0 {
            
            return nil;
            
        }
        
        return variants[0]
        
    }
    
}

public extension SequenceType {
    
    /// Categorises elements of self into a dictionary, with the keys given by keyFunc
    
    func groupBy<U : Hashable>(@noescape keyFunc: Generator.Element -> U) -> [U:[Generator.Element]] {
        var dict: [U:[Generator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}