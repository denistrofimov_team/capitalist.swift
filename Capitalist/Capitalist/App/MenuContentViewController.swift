//
//  MenuContentViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 28.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class MenuContentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton!
    
    var appController:AppViewController?
    var budget:Budget?
    
    @IBOutlet weak var tableView: UITableView!
    
    var user:User? = nil {
    
        didSet {

            if self.viewIfLoaded != nil {
            
                self.fill()
            
            }

        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tableView.registerClass(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "header")
        
        self.settingsButton.setImage(self.settingsButton.imageForState(.Normal)?.tinted, forState: .Normal)
        
        self.headerView.backgroundColor = ApplicationTintColor
        
        self.nameLabel.textColor = UIColor.whiteColor()
        
        self.loginLabel.textColor = UIColor.whiteColor()
        
        self.avatarImageView.image = self.avatarImageView.image?.tinted
        
        self.avatarImageView.tintColor = ApplicationTintColor
        
        self.avatarImageView.backgroundColor = UIColor.whiteColor()
        
        self.view.backgroundColor = ApplicationTintColor
        
        self.tableView.backgroundColor = ApplicationTintColor
        
        self.tableView.tintColor = UIColor.whiteColor()
        
        self.fill()
        
    }
   
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2.0
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        fill()
        
    }
    
    func fill() {
        
        self.tableView.reloadData()
        
        self.nameLabel.text = self.user?.name
        
        self.loginLabel.text = self.user?.login
        
        if self.user?.avatar != nil {
            
            self.avatarImageView.af_setImageWithURL((self.user?.avatar)!)
            
        }
        
    }
    
    //MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 3
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
        
             return 2;
            
        }
        
        if section == 2 {
            
            return 1;
            
        }

        return user != nil ? (user?.budgets.count)! + 1 : 0

    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        
        header.textLabel?.textColor = UIColor.whiteColor()
        header.contentView.backgroundColor = ApplicationTintColor
        
    }
//
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        
//        let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("header")
//        
//        //header!.textLabel!.text = ["Budgets","Settings"][section]
//        
//        return header
//    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")!
        
        cell.tintColor = UIColor.whiteColor()
        cell.backgroundColor = ApplicationTintColor
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.selectionStyle = .None
        
        cell.badgeValue = nil
        
        cell.imageView?.tintColor = UIColor.whiteColor()
        
        if indexPath.section == 1 {
        
            switch indexPath.row {
            case 0:
                
                cell.textLabel?.text = "FAQs".localized()
                cell.imageView?.image = UIImage(named:"ic_help_outline")?.tinted
                
            case 1:
                
                cell.textLabel?.text = "Help".localized()
                cell.imageView?.image = UIImage(named:"ic_chat_bubble_outline")?.tinted
                
                cell.badgeValue = Hotline.sharedInstance().unreadCount() > 0 ? "\(Hotline.sharedInstance().unreadCount())" : nil
                
            default:
                break
            }
            
            return cell
            
        }
        
        if indexPath.section == 2 {
            
            cell.textLabel?.text = "Share with friends".localized()
            cell.imageView?.image = UIImage(named:"ic_share")?.tinted
            
            return cell
            
        }
        
        if indexPath.row == user?.budgets.count {
        
            cell.textLabel?.text = "Add budget".localized();
            cell.imageView?.image = UIImage(named:"ic_add")?.tinted
            
        } else {
        
            let budget = user?.budgets[indexPath.row]
            cell.textLabel?.text = budget!.name;
            cell.imageView?.image = UIImage(named:"ic_inbox")?.tinted
            
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
        
            if indexPath.row < user?.budgets.count {
                
                UIApplication.sharedApplication().activeBudget = self.user?.budgets[indexPath.row]
                
                let controller = UIStoryboard(name: "Budget", bundle: nil).instantiateInitialViewController()
                
                self.revealViewController().pushFrontViewController(controller, animated: true)
                
            } else {
                
                self.revealViewController().revealToggle(tableView.cellForRowAtIndexPath(indexPath))
                
                let alert = UIAlertController(title: "Create new budget".localized(), message: "Enter new budget name".localized(), preferredStyle: .Alert)

                alert.addTextFieldWithConfigurationHandler { (textField) in
                    
                    textField.placeholder = "Name".localized()
                    
                }
                
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .Default, handler: nil))
                
                alert.addAction(UIAlertAction(title: "Add".localized(), style: .Default, handler: { (action) in
                    
                    let name = alert.textFields?.first?.text

                    let budget = Budget()
                    
                    budget.name = name
                    
                    budget.owner = UIApplication.sharedApplication().user
                    
                    budget.users = [UIApplication.sharedApplication().user]
                    
                    budget.save().then({ (budget) -> Void in
                        
                        UIApplication.sharedApplication().user.budgets.append(budget)
                        
                        UIApplication.sharedApplication().createdBudget = budget.id
                        
                        self.revealViewController().dismissViewControllerAnimated(true, completion: nil)
                        
                    }).error({ (error) in
                        
                        print("\(error)")
                        
                    })
                    
                }))
                
                self.revealViewController().presentViewController(alert, animated: true, completion: nil)
                
            }
        
        }
        
        if indexPath.section == 1 {
            
            self.revealViewController().revealToggle(tableView.cellForRowAtIndexPath(indexPath))
            
            switch indexPath.row {
            case 0:

                Hotline.sharedInstance().showFAQs(self.revealViewController())
            
            case 1:
                
                Hotline.sharedInstance().showConversations(self.revealViewController())
                
            default:
                break
            }
            
            
            
        }
        
        if indexPath.section == 2 {
            
             self.revealViewController().revealToggle(tableView.cellForRowAtIndexPath(indexPath))
            
            UIApplication.sharedApplication().share(self.revealViewController())
            
        }
        
    }

    @IBAction func settingsAction(sender: AnyObject) {
        
        let controller = UIStoryboard(name: "Settings", bundle: nil).instantiateInitialViewController()
        
        self.revealViewController().pushFrontViewController(controller, animated: true)
        
    }
    
}
