//
//  MenuViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 26.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import SWRevealViewController

class AppViewController: SWRevealViewController {

    var user:User?
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.frontViewShadowOpacity = 0.3
        self.frontViewShadowOffset = CGSizeMake(0, 0)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "sw_rear" {
            
            
            let menuContent:MenuContentViewController = segue.destinationViewController as! MenuContentViewController
            
            menuContent.user = self.user
            
            
        }
        
    }

}
