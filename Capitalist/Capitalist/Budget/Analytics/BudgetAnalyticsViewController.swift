//
//  AnalyticsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import SwiftMoment

// MARK: - Dataset -

// MARK: CategoryGroup

typealias Groups = [Group]

struct Group {
    
    var name:String
    
    var icon:String?
    
    var color:UIColor?
    
    var iconColor:UIColor?
    
    var categoryIdentifier:Identifier?
    
    var items:Groups = []
    
    var value:Float = 0.0
    
    init(name:String) {
    
        self.name = name
        
    }
    
    init(category:Category, value:Float) {
    
        self.categoryIdentifier = category.id
        
        self.name = category.name!
        
        self.icon = category.icon
        
        self.color = category.color
        
        self.iconColor = category.iconColor
        
        self.value = value
        
    }
    
}

// MARK: - ViewController

class BudgetAnalyticsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var groupBy:TimeUnit = .Months {
    
        didSet {
        
            self.date = moment()
            
        }
        
    }
    
    var date:Moment? {
    
        didSet {
        
            self.proceed()
            
        }
        
    }
    
    var origin:Categories = [] {
    
        didSet {
        
            self.proceed()
            
        }
        
    }
    
    var collection:Groups = [] {
    
        didSet {
        
            if self.tableView != nil {
            
                self.tableView.reloadData()
                
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.date = moment()
        
        self.title = "Analytics".localized()
        
        self.leftButton.tintColor = UIColor.whiteColor()
        
        self.rightButton.tintColor = UIColor.whiteColor()
        
        self.tableView.registerNib(UINib (nibName: "BudgetRecordSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "header")
        
        UIApplication.sharedApplication().activeBudget.getCategories().then({ (collection) -> Void in
            
            self.origin = collection.data
            
        })
        
    }
    
    // MARK: - Proceed -
    
    func proceed() {
        
        var expance = Group(name: "Expence".localized())
        
        var profit = Group(name: "Profit".localized())
        
        for category in self.origin {
            
            let value = self.calculate(category)
            
            if value > 0.0 {
                
                profit.items.append(Group(category:category, value: value))
                
            } else if value < 0.0 {
                
                expance.items.append(Group(category:category, value: value))
                
            }
            
        }
        
        expance.items.sortInPlace({ (a, b) -> Bool in
            
            a.value < b.value
            
        })
        
        profit.items.sortInPlace({ (a, b) -> Bool in
            
            a.value > b.value
            
        })
        
        expance.value = self.calculate(expance)
        profit.value = self.calculate(profit)
        
        self.dateLabel.text = self.dateString()
        
        self.valueLabel.text = NumberUtils.stringFromNumber(expance.value + profit.value)
        
        var result:Groups = []
        
        if expance.items.count > 0 {
        
            result.append(expance)
            
        }
        
        if profit.items.count > 0 {
            
            result.append(profit)
            
        }
        
        self.collection = result
        
    }
    
    // MARK: - Actions -
    
    @IBAction func nextDateAction(sender: IconButton) {
        
        self.date = self.date?.add(1, self.groupBy)
        
    }
    
    @IBAction func previousDateAction(sender: IconButton) {
        
        self.date = self.date?.add(-1, self.groupBy)
        
    }
    
    @IBAction func filterAction(sender: AnyObject) {
    
        let alert = UIAlertController(title: "Group by".localized(), message: "Selecte grouping interval".localized(), preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Day".localized(), style: .Default, handler: { (action) in
        
            self.groupBy = .Days
        
        }))
        
        alert.addAction(UIAlertAction(title: "Month".localized(), style: .Default, handler: { (action) in
            
            self.groupBy = .Months
            
        }))
        
        alert.addAction(UIAlertAction(title: "Quarter".localized(), style: .Default, handler: { (action) in
            
            self.groupBy = .Quarters
            
        }))
        
        alert.addAction(UIAlertAction(title: "Year".localized(), style: .Default, handler: { (action) in
            
            self.groupBy = .Years
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - UITableView -
    
    // MARK: Sections
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.collection.count
    
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header:BudgetRecordSectionHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier("header") as! BudgetRecordSectionHeader
        
        header.firstLabel.text = self.collection[section].name
        
        header.secondLabel.text = NumberUtils.stringFromNumber(self.collection[section].value)
        
        return header
        
    }

    // MARK: Rows
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.collection[section].items.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("category")  as! CategoryTableViewCell
        
        let category = self.collection[indexPath.section].items[indexPath.row]
        
        cell.nameLabel!.text = category.name
        
        cell.valueLabel!.text = NumberUtils.stringFromNumber(category.value)
        
        cell.iconLabel?.icon = category.icon
        
        cell.iconLabel?.backgroundColor = category.color
        
        cell.iconLabel?.textColor = category.iconColor
        
        return cell;
        
    }
    
    //MARK: - Utils
    
    func filter(category:Category) -> Records {
    
        let start = self.date?.startOf(self.groupBy)
        
        let end = self.date?.endOf(self.groupBy)
        
        return category.records.filter { (record) -> Bool in
            
            let date = moment(record.date)
            
            return start < date && date < end
            
        }
        
    }
    
    func calculate(category:Category) -> Float {
        
        return self.calculate(self.filter(category))
        
    }
    
    func calculate(records:Records) -> Float {
        
        return records.reduce(0, combine: { (sum, record) -> Float in
            
            return sum + record.value
            
        })
        
    }
    
    func calculate(group:Group) -> Float {
        
        return group.items.reduce(0, combine: { (sum, category) -> Float in
            
            return sum + category.value
            
        })
        
    }
    
    func dateString() -> String? {
        
        switch self.groupBy {
        case .Days:
            
            return self.date?.format("dd.MM.yyyy")
            
        case .Months:
            
            return self.date?.standaloneMonthName.capitalizedString
            
        case .Quarters:
            
            return self.date?.format("QQQ yyyy")
            
        case .Years:
            
            return self.date?.format("yyyy")
            
        default:
            return self.date?.format()
        }
    
    }
    
}
