//
//  EditCategoryViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import AlamofireImage
import MaterialIconsSwift

class EditCategoryViewController: UIViewController {

    @IBOutlet weak var iconButton: IconButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var iconColorButton: UIButton!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var category:Category? {
    
        didSet {
        
            if self.viewIfLoaded != nil {
                
                self.updateUI()
                
            }
 
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardOnTap = true
        
        self.iconButton.layer.cornerRadius = self.iconButton.frame.size.width / 2
        
        self.colorButton.layer.cornerRadius = 3
        self.iconColorButton.layer.cornerRadius = 3
        
        self.iconColorButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.colorButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.colorButton.layer.borderWidth = 1.0
        self.iconColorButton.layer.borderWidth = 1.0
        
        if self.category == nil {
        
            self.category = Category()
            
            self.category?.budget = UIApplication.sharedApplication().activeBudget
            
        }
        
        self.updateUI()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.category?.name = self.nameTextField.text
        
    }
    
    func updateUI() {
        
        self.title = (self.category!.id != nil ? "Edit Category" : "Create Category").localized()
        
        self.iconButton.backgroundColor = self.category?.color
        
        self.iconButton.setTitleColor(self.category?.iconColor, forState: .Normal)
        
        if self.category?.icon != nil {
        
            self.iconButton.icon = self.category?.icon
            
        }

        self.colorButton.backgroundColor = self.category?.color
        
        self.iconColorButton.backgroundColor = self.category?.iconColor
        
        self.nameTextField.text = self.category?.name
        
    }
    
    @IBAction func unwing(segue:UIStoryboardSegue) {
    
        if segue.identifier == "icon" {
        
            let controller = segue.sourceViewController as! IconsViewController
            
            let icon = controller.icon!
            
            self.category?.icon = icon
            
            self.updateUI()
            
        }
        
        
        if segue.identifier == "color" {
            
            let controller = segue.sourceViewController as! ColorPickerViewController
            
            let color = controller.color!
            
            if controller.target == "icon_color" {
            
                self.category?.iconColor = color
                
            }
            
            if controller.target == "color" {
            
                self.category?.color = color
                
            }

            self.updateUI()
            
        }
    
    }
    
    @IBAction func doneAction(sender: AnyObject) {
        
        self.category?.name = self.nameTextField.text
        
        if self.category?.budget == nil {
        
            self.category?.budget = UIApplication.sharedApplication().activeBudget
            
        }
        
        if self.category!.valid() {
            
            self.category!.save().then({ (category) ->Void in
                
                if UIApplication.sharedApplication().activeBudget.categories[category.id!] == nil {
                
                    UIApplication.sharedApplication().activeBudget.categories.append(category)
                    
                }
                
                self.navigationController?.popViewControllerAnimated(true)
                
            })
            
        } else {
        
            print("\(self.category?.invalid())")
            
        }
        
    }

    
    // MARK: - Navigation

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "color" {
            
            let navigationController = segue.destinationViewController as! UINavigationController
            
            let controller = navigationController.topViewController as! ColorPickerViewController
            
            controller.color = self.category?.color
            
            controller.target = "color"
            
        }
        
        if segue.identifier == "icon_color" {
            
            let navigationController = segue.destinationViewController as! UINavigationController
            
            let controller = navigationController.topViewController as! ColorPickerViewController
            
            controller.color = self.category?.iconColor
            
            controller.target = "icon_color"
            
        }
        
    }

}
