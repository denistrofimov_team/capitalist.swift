//
//  ContactsTableViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 04.08.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DigitsKit
import DZNEmptyDataSet
import Branch

class ContactsTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var users:Users? = nil
    var selected:Users = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
        
    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let userSession = Digits.sharedInstance().session()
        
        let contacts = DGTContacts(userSession: userSession)
        
        let appearance = DGTAppearance()
        
        appearance.accentColor = ApplicationTintColor
        
        appearance.logoImage = UIImage(named: "logotype")
        
        self.title = "Loading...".localized()
        
        contacts.startContactsUploadWithDigitsAppearance(appearance, presenterViewController: self, title: nil) { result, error in
            
            if error != nil {
                
                return self.alert(error)
                
            }
            
            contacts.lookupContactMatchesWithCursor(nil) { matches, nextCursor, error in
                
                if error != nil {
                    
                    return self.alert(error)
                    
                }
                
                let ids = matches.map({ (match) -> String in
                    
                    return (match as! DGTUser).userID
                    
                })
                
                let except = UIApplication.sharedApplication().activeBudget.users.map({ (user) -> String in
                    
                    return user.id!
                    
                })
                
                User.contacts(ids, except:except).then({ (users) -> Void in
                    
                    self.users = users
                    
                    if !(self.users?.isEmpty)! {
                    
                        self.doneButton.enabled = true
                        
                    }
                    
                    self.tableView.reloadData()
                    
                    self.title = "Contacts".localized()
                    
                })
                
            }
            
        }
        
    }
    
    // MARK: - Table view data source

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let users = self.users else {return 0}
        
        return users.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("user", forIndexPath: indexPath) as! UserTableViewCell
        
        let user = self.users![indexPath.row]
        
        cell.nameLabel?.text = user.displayName
        
        cell.avatarImageView.af_setImageWithURL(user.avatar!)
        
        cell.accessoryType = self.selected.contains(user) ? .Checkmark : .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        let user = self.users![indexPath.row]
        
        if self.selected.contains(user) {
            
            cell?.accessoryType = .None
            
            self.selected.removeAtIndex(self.selected.indexOf(user)!)
            
        } else {
        
            cell?.accessoryType = .Checkmark
            
            self.selected.append(user)
            
        }
        
    }
    
    // MARK: - Empty Dataset -

    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No matches".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no users in your phone book that use Capitalist".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Share with friends".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        return self.users != nil
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        UIApplication.sharedApplication().share(self)
        
    }
    
}
