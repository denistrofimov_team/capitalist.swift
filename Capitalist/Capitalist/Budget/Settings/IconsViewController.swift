//
//  IconsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MaterialIconsSwift

// MARK: Reusable views

class IconCellView: UICollectionViewCell {
    
    @IBOutlet weak var iconLabel: UILabel!
    
}

// MARK: IconsViewController

class IconsViewController: UICollectionViewController, UITextFieldDelegate {
    
    var icons = MaterialIcons.icons()
    
    var original = MaterialIcons.icons()
    
    var icon:String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Icons".localized()
        
    }

    // MARK: UICollectionViewDataSource
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.icons.count
        
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("icon", forIndexPath: indexPath) as! IconCellView
        
        cell.iconLabel.font = MaterialIcons.fontOfSize(24)
        
        cell.iconLabel.text = MaterialIcons.icon(self.icons[indexPath.row])
        
        cell.iconLabel.textColor = ApplicationTintColor
        
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "icon" {
            
            if let cell = sender as? IconCellView, let indexPath = collectionView!.indexPathForCell(cell) {
                
                self.icon = self.icons[indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - UItextField -

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var txtAfterUpdate:NSString = textField.text! as NSString
        
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        
        self.search(txtAfterUpdate as String)
        
        return true
        
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        
        self.icons = self.original
        
        self.collectionView?.reloadData()
        
        return true
        
    }
    
    // MARK - Search -
    
    func search(token:String) {
        
        if token.characters.count > 0 {
            
            self.icons = self.original.filter({ (icon) -> Bool in
                
                return icon.containsString(token.lowercaseString)
                
            })
            
        } else {
        
            self.icons = self.original
            
        }
        
        self.collectionView?.reloadData()
        
    }
    
}
