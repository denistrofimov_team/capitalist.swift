//
//  AccessSettingsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DigitsKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.avatarImageView.layer.cornerRadius = 38/2
        self.avatarImageView.clipsToBounds = true
        
        
    }
    
}

class AccessSettingsViewController: UITableViewController {

    @IBOutlet weak var addButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return UIApplication.sharedApplication().activeBudget.users.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("user", forIndexPath: indexPath) as! UserTableViewCell
        
        let user = UIApplication.sharedApplication().activeBudget.users[indexPath.row]
        
        cell.nameLabel?.text = user.displayName
        
        cell.loginLabel.text = UIApplication.sharedApplication().activeBudget.owner?.id == user.id ? "Owner".localized() : "Member".localized()
        
        cell.avatarImageView.af_setImageWithURL(user.avatar!)
        
        return cell
    }

    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete".localized()) { action, indexPath in
            
            self.confirm("Delete?".localized(), message: "Are you sure?".localized(), clouser: {
                
                let user = UIApplication.sharedApplication().activeBudget.users[indexPath.row]
                
                UIApplication.sharedApplication().activeBudget.removeUser(user).then({ () -> Void in
                    
                    UIApplication.sharedApplication().activeBudget.users.removeAtIndex(indexPath.row)
                    
                    tableView.beginUpdates()
                    
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    
                    tableView.endUpdates()
                    
                })

            })
            
        }
        
        return [delete]
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
        
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    @IBAction func unwing(segue:UIStoryboardSegue) {
        
        if segue.identifier == "users" {
            
            let controller = segue.sourceViewController as! ContactsTableViewController
            
            UIApplication.sharedApplication().activeBudget.users.appendContentsOf(controller.selected)
            
            let ids = controller.selected.map({ (user) -> String in
                
                return user.id!
                
            })

            UIApplication.sharedApplication().activeBudget.share(users:ids).error(self.alert)
            
            self.tableView.reloadData()
            
        }
        
    }

}
