//
//  ColorPIckerViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 20.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import Color_Picker_for_iOS

class ColorPickerViewController: UIViewController {
    
    @IBOutlet weak var colorMap: HRColorMapView!
    @IBOutlet weak var colorPickerView: HRColorPickerView!
    
    var color:UIColor? {

        didSet {
            
            if colorPickerView != nil {
                
                colorPickerView.color = color
                
            }
            
        }
        
    }
    var target:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select color".localized()
        
        colorPickerView.color = color
        
    }
    
    @IBAction func colorChanged(sender: HRColorPickerView) {
        
        self.color = sender.color
        
    }
}
