//
//  BudgetSettingsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PageMenu
import UIColor_Hex_Swift

class BudgetSettingsViewController: UITableViewController {

    @IBOutlet weak var generalIcon: UIImageView!
    @IBOutlet weak var categoriesIcon: UIImageView!
    @IBOutlet weak var accessIcon: UIImageView!
    @IBOutlet weak var walletsIcon: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Settings".localized()
     
        self.generalIcon.image = self.generalIcon.image?.tinted
        self.categoriesIcon.image = self.categoriesIcon.image?.tinted
        self.accessIcon.image = self.accessIcon.image?.tinted
        self.walletsIcon.image = self.walletsIcon.image?.tinted
        
        self.generalIcon.tintColor = ApplicationTintColor
        self.categoriesIcon.tintColor = ApplicationTintColor
        self.accessIcon.tintColor = ApplicationTintColor
        self.walletsIcon.tintColor = ApplicationTintColor
        
    }

}
