//
//  EditWalletViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class EditWalletViewController: UIViewController {

    var wallet:Wallet? {
    
        didSet {
            
            if self.viewIfLoaded != nil {
                
                self.updateUI()
                
            }
            
        }
        
    }
  
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var userButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardOnTap = true
        
        if self.wallet == nil {
            
            self.wallet = Wallet()
            
            self.wallet?.budget = UIApplication.sharedApplication().activeBudget
            
        }
        
        if self.wallet?.user == nil {
            
            self.wallet?.user = UIApplication.sharedApplication().user
            
        }

        self.updateUI()
        
    }
    
    func updateUI() {
        
        self.title = (self.wallet!.id != nil ? "Edit Wallet" : "Create Wallet").localized()
        
        if self.nameTextField.text == nil {
        
            self.nameTextField.text = self.wallet!.name
            
        }
        
        if self.wallet!.user != nil {
            
            self.userButton.setTitle(self.wallet!.user!.displayName, forState: .Normal)
            
        }
        
    }
    
    @IBAction func unwing(segue:UIStoryboardSegue) {
    
        if segue.identifier == "user" {
            
            let controller = segue.sourceViewController as! UsersTableViewController
            
            self.wallet!.user = controller.user
            
            self.updateUI()
            
        }
    
    }
    
    @IBAction func doneAction(sender: UIBarButtonItem) {
        
        self.wallet!.name = self.nameTextField.text
        
        if self.wallet?.budget == nil {
            
            self.wallet?.budget = UIApplication.sharedApplication().activeBudget
            
        }
        
        if self.wallet!.valid() {
            
            self.wallet!.save().asVoid().then({ (wallet) in
                
                if UIApplication.sharedApplication().activeBudget.wallets[(self.wallet?.id)!] == nil {
                
                    UIApplication.sharedApplication().activeBudget.wallets.append(self.wallet!)
                    
                }
                
                self.navigationController?.popViewControllerAnimated(true)
                
            }).error({ (error) in
                
                print("\(error)")
                
            })
            
        }
        
        
    }

}
