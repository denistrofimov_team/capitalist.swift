//
//  WalletsSettingsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class WalletsSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource  {

    @IBOutlet weak var tableView:UITableView?
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tableView?.emptyDataSetDelegate = self
        
        self.tableView?.emptyDataSetSource = self
        
        self.tableView?.reloadData()
        
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UIApplication.sharedApplication().activeBudget.wallets.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("wallet", forIndexPath: indexPath)
        
        let wallet = UIApplication.sharedApplication().activeBudget.wallets[indexPath.row]

        cell.textLabel!.text = wallet.name
        
        cell.detailTextLabel?.text = wallet.user?.displayName
        
        return cell
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete".localized()) { action, indexPath in
            
            self.confirm("Delete?".localized(), message: "Are you sure?".localized(), clouser: {
                
                let wallet = UIApplication.sharedApplication().activeBudget.wallets[indexPath.row]
                
                wallet.delete().asVoid().then({ (category) in
                    
                    UIApplication.sharedApplication().activeBudget.wallets.removeAtIndex(indexPath.row)
                    
                    tableView.beginUpdates()
                    
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    
                    tableView.endUpdates()
                    
                })
                
            })
            
        }
        
        return [delete]
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "wallet", let destination = segue.destinationViewController as? EditWalletViewController {
            
            if let cell = sender as? UITableViewCell, let indexPath = self.tableView!.indexPathForCell(cell) {
                
                destination.wallet = UIApplication.sharedApplication().activeBudget.wallets[indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - Empty Dataset -
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No wallets".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no wallets in your budget".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Add wallet".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        return UIApplication.sharedApplication().activeBudget.categories.count == 0
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        self.performSegueWithIdentifier("add_wallet", sender: scrollView)
        
    }

}
