//
//  UsersTableViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 07.08.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class UsersTableViewController: UITableViewController {

    var user:User?
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return UIApplication.sharedApplication().activeBudget.users.count
        
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("user", forIndexPath: indexPath) as! UserTableViewCell

        let user = UIApplication.sharedApplication().activeBudget.users[indexPath.row]
        
        if user.avatar != nil {
            
            cell.avatarImageView.af_setImageWithURL(user.avatar!)
            
        }
        
        cell.nameLabel.text = user.displayName
        
        cell.loginLabel.text = user.login

        return cell
    }

    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "user" {
            
            if let cell = sender as? UITableViewCell, let indexPath = self.tableView!.indexPathForCell(cell) {
                
                self.user = UIApplication.sharedApplication().activeBudget.users[indexPath.row]
                
            }
        }
        
    }
    

}
