//
//  CategoriesSettingsVTableViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class CategoryTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        
        super.awakeFromNib()

        self.selectionStyle = .None
        
    }
    
    @IBOutlet weak var iconLabel: IconLabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
}

class CategoriesSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {

    @IBOutlet weak var tableView:UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView?.emptyDataSetDelegate = self
        
        self.tableView?.emptyDataSetSource = self

    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tableView?.reloadData()
        
    }

    // MARK: - Table view data source

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UIApplication.sharedApplication().activeBudget.categories.count
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("category", forIndexPath: indexPath) as! CategoryTableViewCell

        let category = UIApplication.sharedApplication().activeBudget.categories[indexPath.row]
        
        cell.nameLabel!.text = category.name

        cell.iconLabel.icon = category.icon
        
        cell.iconLabel.backgroundColor = category.color
        
        cell.iconLabel.textColor = category.iconColor
        
        return cell
    }

    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete".localized()) { action, indexPath in
            
            self.confirm("Delete?".localized(), message: "Are you sure?".localized(), clouser: {
                
                let category = UIApplication.sharedApplication().activeBudget.categories[indexPath.row]
                
                category.delete().asVoid().then({ (category) in
                    
                    UIApplication.sharedApplication().activeBudget.categories.removeAtIndex(indexPath.row)
                    
                    tableView.beginUpdates()
                    
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    
                    tableView.endUpdates()
                    
                })
                
            })
            
        }
        
        return [delete]
        
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "category", let destination = segue.destinationViewController as? EditCategoryViewController {
            
            if let cell = sender as? UITableViewCell, let indexPath = self.tableView!.indexPathForCell(cell) {
                
                destination.category = UIApplication.sharedApplication().activeBudget.categories[indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - Empty Dataset -
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No categories".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no categories in your budget".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Add category".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        return UIApplication.sharedApplication().activeBudget.categories.count == 0
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        self.performSegueWithIdentifier("add_category", sender: scrollView)
        
    }

}
