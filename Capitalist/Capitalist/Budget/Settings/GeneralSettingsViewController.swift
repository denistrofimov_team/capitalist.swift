//
//  GeneralSettingsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 17.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit

class GeneralSettingsViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var removeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardOnTap = true
        
        self.title = UIApplication.sharedApplication().activeBudget.name
        
        self.updateUI()
        
    }
    
    func updateUI() {
        
        self.nameTextField.text = UIApplication.sharedApplication().activeBudget.name
        
        self.removeButton.enabled = UIApplication.sharedApplication().activeBudget.id != UIApplication.sharedApplication().user.primaryBudget!.id
        
    }
    
    @IBAction func removeAction(sender: AnyObject) {
        
        self.confirm("Delete?".localized(), message: "Are you sure?".localized()) { 

            UIApplication.sharedApplication().activeBudget.delete()
            
            self.revealViewController().dismissViewControllerAnimated(true, completion: nil)
            
        }
        
    }
    
    @IBAction func doneAction(sender: AnyObject) {
        
        UIApplication.sharedApplication().activeBudget.name = self.nameTextField.text
        
        if UIApplication.sharedApplication().activeBudget.valid() {
        
            UIApplication.sharedApplication().activeBudget.save().then { (budget) -> Void in
                
                self.navigationController?.popViewControllerAnimated(true)
                
            }
            
        }
        
    }
}
