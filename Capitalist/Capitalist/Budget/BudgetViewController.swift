//
//  BudgetViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 13.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

extension UIViewController {
    
    @IBAction func menuAction(sender: AnyObject) {
        
        let source = self.revealViewController()
        
        source.revealToggle(sender)
        
    }
    
}

class BudgetViewController: UITabBarController {

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.removeGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }
    
}
