//
//  BudgetRecordViewController.swift
//  Capitalist
//
//  Created by Денис Трофимов on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import VENCalculatorInputView
import RKTagsView

class BudgetRecordViewController: UIViewController, UITextFieldDelegate, RKTagsViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var valueTextField: VENCalculatorInputTextField!
    @IBOutlet var inputToolbar: UIToolbar!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var tagsView: RKTagsView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var doneButton: UIBarButtonItem!

    // Properties
    
    var date:NSDate? {
    
        didSet {
        
            if self.dateButton != nil {
            
                self.dateButton.setTitle(date != nil ? DateUtils.humanize(date!) : "Date".localized(), forState: .Normal)
            
            }
            
        }
        
    }
    
    var category:Category? {
        
        didSet {
            
            if self.categoryButton != nil {
                
                self.categoryButton.setTitle(category != nil ? category?.name : "Category".localized(), forState: .Normal)
                
            }
            
        }
        
    }
    
    var wallet:Wallet? {
        
        didSet {
            
            if self.walletButton != nil {
                
                let title = wallet != nil ? "\(wallet!.name!) / \(wallet!.user!.displayName)" : "Wallet".localized()
                
                self.walletButton.setTitle(title, forState: .Normal)
                
            }
            
        }
        
    }
    
    var record:Record? {
    
        didSet {
            
            let view = self.viewIfLoaded
            
            if(view != nil) {
            
                fill(record)
                
            }
 
        }
        
    }
    
    // MARK: - View management -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.valueTextField.locale = NSLocale.currentLocale()
        
        self.categoryButton.setImage(UIImage(imageLiteral:"ic_folder_open").tinted, forState: .Normal)
        self.categoryButton.setTitleColor(ApplicationTintColor, forState: .Normal)
        
        self.dateButton.setImage(UIImage(imageLiteral:"ic_event").tinted, forState: .Normal)
        self.dateButton.setTitleColor(ApplicationTintColor, forState: .Normal)
        
        self.walletButton.setImage(UIImage(imageLiteral:"ic_account_balance_wallet").tinted, forState: .Normal)
        self.walletButton.setTitleColor(ApplicationTintColor, forState: .Normal)
        
        self.tagsView.deliminater = NSCharacterSet.newlineCharacterSet()
        
        self.tagsView.textField.delegate = self
        
        self.tagsView.tintColor = UIApplication.sharedApplication().keyWindow?.tintColor
        
        self.tagsView.textField.placeholder = "Tags".localized()
        
        let tagsHeight:CGFloat = 28
        
        self.tagsView.textFieldHeight = tagsHeight
        
        self.tagsView.tagButtonHeight = tagsHeight
        
        self.tagsView.lineSpacing = 8
        
        fill(self.record)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.valueTextField.becomeFirstResponder()
        
    }
    
    // MARK: - Tags management -
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        let text = textField.text;
        
        textField.text = nil;
        
        if (text != nil) && (text?.characters.count > 0) {
        
            self.tagsView.addTag(text!)
            
        }
        
        return false
        
    }
    
    func tagsView(tagsView: RKTagsView, buttonForTagAtIndex index: Int) -> UIButton {
        
        let button = UIButton()
        
        button.tintColor = UIApplication.sharedApplication().keyWindow?.tintColor

        button.setTitle(tagsView.tags[index], forState: .Normal)
        
        button.setTitleColor(UIApplication.sharedApplication().keyWindow?.tintColor, forState: .Normal)
        
        button.sizeToFit()
        
        return button;
        
    }

    // MARK: - UI -
    
    func fill(record:Record?) {
        
        if record != nil  {
        
            self.valueTextField.text = "\(abs(record!.value!))"

            self.date = record?.date;
            
            self.category = record?.category
            
            self.wallet = record?.wallet
            
            self.tagsView.removeAllTags()
            
            for tag in (record?.tags)! {
                
                self.tagsView.addTag(tag)
                
            }
            
            self.segmentControl.selectedSegmentIndex = record?.value < 0.0 ? 0 : 1
            
        } else {
        
            self.date = NSDate()
            
            self.segmentControl.selectedSegmentIndex = 0
            
            self.valueTextField.text = nil
            
            self.tagsView.removeAllTags()
            
            self.category = UIApplication.sharedApplication().activeBudget.categories.first
            
            self.wallet = UIApplication.sharedApplication().activeBudget.wallets.first

        }
        
    }
    
    func save() {
        
        self.valueTextField.resignFirstResponder()
        
        let record = Record()
        
        if self.record != nil {

            record.id = self.record?.id
            
        }
        
        let value = self.valueTextField.text!
        
        record.value = abs(NumberUtils.numberFromString(value)) * (self.segmentControl.selectedSegmentIndex == 0 ? -1.0 : 1.0)
        
        record.date = self.date
        
        record.category = self.category
        
        record.wallet = self.wallet
        
        record.tags = self.tagsView.tags
        
        record.budget = UIApplication.sharedApplication().activeBudget
        
        record.user = UIApplication.sharedApplication().user
        
        if record.valid() {
        
            self.record = record
            
            record.save().asVoid().then({ (record) in
                
                self.navigationController?.popViewControllerAnimated(true)
                
            })
        
        }
        
    }
    
    // MARK: - Navigation -
    
    @IBAction func unwing(segue: UIStoryboardSegue) {
        
        if segue.identifier == "calendar" {
            
            let source = segue.sourceViewController as! CalendarViewController
            
            self.date = source.date
            
        }
        
        if segue.identifier == "categories" {
            
            let source = segue.sourceViewController as! CategoriesViewController
            
            self.category = source.category
            
        }
        
        if segue.identifier == "wallets" {
            
            let source = segue.sourceViewController as! WalletsTableViewController
            
            self.wallet = source.wallet
            
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "date" {
            
            let destination = segue.destinationViewController as! CalendarViewController
            
            destination.date = self.date
            
        }
        
    }
    
    // MARK: - Actions -
    
    @IBAction func hideKeyboardAction(sender: AnyObject) {
        
        self.valueTextField.resignFirstResponder()
        
    }
    
    

    @IBAction func doneAction(sender: AnyObject) {
        
        self.save()
        
    }
}
