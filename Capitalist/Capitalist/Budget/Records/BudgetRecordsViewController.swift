//
//  ExampleViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 26.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import PromiseKit
import DZNEmptyDataSet

struct RecordGroup {
    
    var date:NSDate
    var title:String
    var records:[Record]
    var sum:Float
    
    mutating func calculate() -> Float {
        
        self.sum = self.records.reduce(0, combine: { (sum, record) -> Float in
            
            return sum + record.value
            
        })

        return self.sum
        
    }
    
}

class BudgetRecordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var iconLabel: IconLabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.avatarImageView.layer.cornerRadius = 8
        
        self.avatarImageView.clipsToBounds = true
        
    }
    
}


class BudgetRecordsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var collection:[RecordGroup]? {
    
        didSet {
        
            let sum:Float = collection!.reduce(0.0) { (sum, group) -> Float in
                
                return sum + group.sum
                
            }
            
            self.valueLabel.text = NumberUtils.stringFromNumber(sum)
            
            UIView.animateWithDuration(0.25) { 
                
                self.valueLabel.hidden = false
                
            } 
            
            if self.tableView != nil {
            
                self.tableView.reloadData()
                
            }
            
        }
        
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.activeBudgetChanged(UIApplication.sharedApplication().activeBudget)

        self.tableView.registerNib(UINib (nibName: "BudgetRecordSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "header")
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(BudgetRecordsViewController.refresh(_:)), forControlEvents: .ValueChanged)
        
        tableView.addSubview(refreshControl)
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(BudgetRecordsViewController.didBecomeActive(_:)),
                                                         name: UIApplicationDidBecomeActiveNotification,
                                                         object: nil)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let selection = self.tableView.indexPathForSelectedRow;
        
        if selection != nil {
            
            self.tableView.deselectRowAtIndexPath(selection!, animated: true)
            
        }
        
        self.activeBudgetChanged(UIApplication.sharedApplication().activeBudget)
        
        self.checkURL()
        
    }
    
    func didBecomeActive(notification:NSNotification){
        
        self.checkURL()
        
    }
    
    func checkURL() {
        
        guard let url = UIApplication.sharedApplication().startURL else {
            
            return
            
        }
        
        if url.entity == "record" {
            
            UIApplication.sharedApplication().startURL = nil
            
            self.performSegueWithIdentifier("create-record", sender: nil)
            
        }
        
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        
        UIApplication.sharedApplication().activeBudget.getRecords().then { (records) -> Void in
            
            self.collection = self.group(records.data)
            
            }.always {
                
                refreshControl.endRefreshing()
                
        }

    }

    
    func activeBudgetChanged(budget: Budget!) {
        
        if budget == nil {
            
            return
            
        }
        
        self.titleLabel.text = UIApplication.sharedApplication().activeBudget.name
        self.valueLabel.text = nil;
        
        UIView.animateWithDuration(0.25) {
            
            self.valueLabel.hidden = true
            
        }
        
        UIApplication.sharedApplication().activeBudget.getRecords().then { (records) -> Void in
            
            self.collection = self.group(records.data)
            
        }

    }
    
    // MARK: - Utils
    
    func group(input:[Record]) -> [RecordGroup] {
        
        let grouped = input.groupBy { (record) -> String in
            
            return DateUtils.shortDateString(record.date)
            
        }
        
        var result:[RecordGroup] = []
        
        for (dateString, records) in grouped {
            
            let date = DateUtils.shortDate(dateString)
            
            let sum = records.reduce(0, combine: { (sum, record) -> Float in
                
                return sum + record.value
                
            })
            
            result.append(RecordGroup(date:date, title: DateUtils.humanize(date), records: records, sum: sum))
            
        }
        
        return result.sort({ (a, b) -> Bool in
            
            return a.date.timeIntervalSince1970 > b.date.timeIntervalSince1970
            
        })
        
    }
    
    // MARK: - UITableView -
    
    // MARK: Data
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.collection == nil {
        
            return 0
            
        }
        
        return self.collection!.count
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.collection![section].records.count
        
    }
    
    // MARK: Headers
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header:BudgetRecordSectionHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier("header") as! BudgetRecordSectionHeader
        
        header.firstLabel.text = self.collection![section].title
        
        header.secondLabel.text = NumberUtils.stringFromNumber(self.collection![section].sum)
        
        return header
        
    }
    
    // MARK: Cells
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        let record = self.collection![indexPath.section].records[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("record") as! BudgetRecordTableViewCell
        
        cell.categoryLabel?.text = record.category.name
        
        cell.userLabel.text = record.wallet != nil ? record.wallet.name! : record.user.displayName
        
        cell.valueLabel.text = NumberUtils.stringFromNumber(record.value)
        
        cell.iconLabel.icon = record.category.icon
        
        cell.iconLabel.backgroundColor = record.category.color
        
        cell.iconLabel.textColor = record.category.iconColor
        
        if record.wallet != nil && record.wallet.user!.avatar != nil {
        
            cell.avatarImageView.af_setImageWithURL(record.wallet.user!.avatar!, placeholderImage: UIImage.empty())
            
        } else if record.user.avatar != nil {
        
            cell.avatarImageView.af_setImageWithURL(record.user!.avatar!, placeholderImage: UIImage.empty())
            
        } else {
            
            cell.avatarImageView.image = nil
            
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete".localized()) { action, indexPath in

            self.confirm("Удалить?", message: "Вы уверены?", clouser: {
                
                let record = self.collection![indexPath.section].records[indexPath.row]
                
                record.delete().asVoid().then({ (record) in
                    
                    tableView.beginUpdates()
                    
                    if self.collection![indexPath.section].records.count == 1 {
                        
                        self.collection!.removeAtIndex(indexPath.section)
                        
                        tableView.deleteSections(NSIndexSet(index: indexPath.section), withRowAnimation: .Automatic)
                        
                    } else {
                        
                        self.collection![indexPath.section].records.removeAtIndex(indexPath.row)
                        
                        self.collection![indexPath.section].calculate()
                        
                        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                        
                    }
                    
                    tableView.endUpdates()
                    
                })
                
            })

        }
        
        return [delete]
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "record", let destination = segue.destinationViewController as? BudgetRecordViewController {
            
            if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPathForCell(cell) {
                
                destination.record = self.collection![indexPath.section].records[indexPath.row]
                
            }
        }
        
    }
    
    // MARK: - Empty Dataset -
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "No records".localized(),
                                  attributes: [:])
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "There is no records in your budget".localized(),
                                  attributes: [:])
        
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        return NSAttributedString(string: "Add record".localized(),
                                  attributes: [NSForegroundColorAttributeName:ApplicationTintColor])
        
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        
        if self.collection == nil {
            
            return false
            
        }
        
        return self.collection!.count == 0
        
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        self.performSegueWithIdentifier("create-record", sender: scrollView)
        
    }
    
    // MARK: - 
    
    deinit {
    
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
    
    
}
