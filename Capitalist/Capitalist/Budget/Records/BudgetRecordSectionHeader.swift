//
//  BudgetRecordSectionHeader.swift
//  Capitalist
//
//  Created by Денис Трофимов on 08.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class BudgetRecordSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBOutlet weak var container: UIView!
    
    override var contentView: UIView {
    
        return container
        
    }
    
}
