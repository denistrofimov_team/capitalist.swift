//
//  BudgetsTableViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 05.08.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit

class BudgetsTableViewController: UITableViewController {

    var budget:Budget? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return UIApplication.sharedApplication().user.budgets.count
    
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("budget")
        
        cell?.textLabel?.text = UIApplication.sharedApplication().user.budgets[indexPath.row].name
        
        return cell!
        
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier == "budget" {
            
            if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPathForCell(cell) {
                
                self.budget = UIApplication.sharedApplication().user.budgets[indexPath.row]
                
            }
        }
        
        
    }

}
