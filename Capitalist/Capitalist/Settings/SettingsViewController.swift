//
//  SettingsViewController.swift
//  Capitalist
//
//  Created by Трофимов Денис on 14.07.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import DBCamera
import DigitsKit

class SettingsViewController: UIViewController, DBCameraViewControllerDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var primaryBudgetButton: UIButton!
    
    var user:User? {
    
        didSet {
        
            if self.viewIfLoaded != nil {
                
                self.fill()
                
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "App Settings".localized()
        
        self.avatarImageView.layer.borderWidth = 0.5
        self.avatarImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.avatarImageView.layer.cornerRadius = 35.0
        self.avatarImageView.tintColor = UIColor.lightGrayColor()
        
        User.current().then { (user) -> Void in
            
            self.user = user
            
        }
        
    }
    
    func fill() {
        
        nameTextField.text = user?.name
        
        primaryBudgetButton.setTitle(user?.primaryBudget?.name, forState: .Normal)
        
        if self.user?.avatar != nil {
        
            avatarImageView.af_setImageWithURL((self.user?.avatar)!)
            
        }

    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.removeGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }

    @IBAction func logoutAction(sender: AnyObject) {
        
        self.confirm("Sign out".localized(), message: "Are you sure?".localized()) {
            
            Manager.post("authenticator/signout")
                .then { (response:JSONObject) ->Void in
                    
                    Manager.clearAccessToken()
                    
                    let digits = Digits.sharedInstance()
                    
                    digits.logOut()
                    
                    UIApplication.sharedApplication().user = nil
                    
                    self.revealViewController().dismissViewControllerAnimated(true, completion: nil)
                    
                }
                .error(self.alert)
            
        }
 
    }
    
    @IBAction func doneAction(sender: AnyObject) {
        
        self.user!.name = self.nameTextField.text
        
        self.user!.save().then { (user) -> Void in
            
            self.alert("Done".localized(), message: "Settings saved".localized())
            
        }.error(self.alert)
        
    }
    
    // MARK: - Camera -
    
    @IBAction func avatarAction(sender: UITapGestureRecognizer) {
        
        let cameraController = DBCameraViewController.initWithDelegate(self);
        
        cameraController.forceQuadCrop = true
        cameraController.selectedTintColor = ApplicationTintColor
        
        let cameraContainer = DBCameraContainerViewController(delegate: self)
        cameraContainer.cameraViewController = cameraController
        cameraContainer.setFullScreenMode()
        
        let nav = UINavigationController(rootViewController:cameraContainer)
        nav.navigationBarHidden = true;
        
        self.presentViewController(nav, animated: true, completion: nil)
        
    }
    
    func camera(cameraViewController: AnyObject!, didFinishWithImage image: UIImage!, withMetadata metadata: [NSObject : AnyObject]!) {
        
        self.avatarImageView.image = image
        
        User.uploadAvatar(image).then { (url) -> Void in
            
            self.avatarImageView.af_setImageWithURL(url)
            
            self.user!.avatar = url
            
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func dismissCamera(cameraViewController: AnyObject!) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    // MARK: - Actions -
    
    @IBAction func assignPhoneNumber(sender: AnyObject) {
        
        let digits = Digits.sharedInstance()
        
        let configuration = DGTAuthenticationConfiguration(accountFields: .DefaultOptionMask)
        
        let appearance = DGTAppearance()
        
        appearance.accentColor = ApplicationTintColor
        
        appearance.logoImage = UIImage(named: "logotype")
        
        configuration.appearance = appearance
        
        digits.authenticateWithViewController(nil, configuration:configuration) { (session, error) in
            
            if(error != nil) {
                
                return self.alert(error!)
                
            }
            
            self.user!.digitsID = session.userID
            
            self.user!.save()
            
        }
        
    }
    
    @IBAction func unwing(segue: UIStoryboardSegue) {
        
        if segue.identifier == "budget" {
            
            let source = segue.sourceViewController as! BudgetsTableViewController
            
            self.user?.primaryBudget = source.budget
            
            primaryBudgetButton.setTitle(user?.primaryBudget?.name, forState: .Normal)
            
        }
        
    }
    
}
