//
//  AppDelegate.swift
//  Capitalist
//
//  Created by Денис Трофимов on 25.06.16.
//  Copyright © 2016 Денис Трофимов. All rights reserved.
//

import UIKit
import ObjectiveC
import Fabric
import Crashlytics
import DigitsKit
import Branch

class StartURL {
    
    var entity:String
    var path:[String] = []
    var query:Parameters? = nil
    
    init(entity:String, path:[String] = [], query:Parameters? = nil){
        
        self.entity = entity
        
        self.path = path
        
        self.query = query
        
    }
    
}


var StartURLHandle: UInt8 = 2
let StartURLChangeNotification = "StartURLChangeNotification"

extension UIApplication {
    
    var startURL:StartURL? {
        
        get {
            
            return objc_getAssociatedObject(self, &StartURLHandle) as? StartURL
            
        }
        
        set {
            
            objc_setAssociatedObject(self, &StartURLHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            NSNotificationCenter.defaultCenter().postNotificationName(StartURLChangeNotification, object: newValue)
            
        }
        
    }
    
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceToken: String?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Branch.getInstance().initSessionWithLaunchOptions(launchOptions, andRegisterDeepLinkHandler: { params, error in
            guard error != nil else { return }
            guard let userDidClick = params?["+clicked_branch_link"] as? Bool else { return }
            if userDidClick {
                
                
                
            }
        })

        
        // Manager settings
        //Manager.setBaseURL("http://localhost:7051")
        Manager.setBaseURL("http://api.capitalistapp.com");
        
        let hotlineConfig = HotlineConfig(appID: "2ffca6ce-d9c6-4530-8637-0b6e38c23723",
                                          andAppKey: "92e6b6a2-ac11-4dcf-9ff1-3655a85e0ed7")
        
        hotlineConfig.themeName = "theme"
        
        Hotline.sharedInstance().initWithConfig(hotlineConfig)
        
        self.window?.tintColor = ApplicationTintColor
        
        UINavigationBar.appearance().barTintColor = self.window?.tintColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:.Normal)
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics:.Default)
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics:.Default)
        
        let image = UIImage(imageLiteral:"ic_arrow_back")
        
        UINavigationBar.appearance().backIndicatorImage = image
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = image
        
        application.statusBarStyle = .LightContent
        
        if Hotline.sharedInstance().isHotlineNotification(launchOptions) {
            
            Hotline.sharedInstance().handleRemoteNotification(launchOptions, andAppstate: application.applicationState)
            
        }
        
        Fabric.with([Crashlytics.self, Digits.self, Branch.self])
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        self.deviceToken = deviceToken.description.componentsSeparatedByCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet).joinWithSeparator("")
        
        Hotline.sharedInstance().updateDeviceToken(deviceToken)
        
        User.current().then { (user) -> Void in
            
            user.registerDeviceToken(self.deviceToken!)
            
        }
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
  
        if Hotline.sharedInstance().isHotlineNotification(userInfo) {
        
            Hotline.sharedInstance().handleRemoteNotification(userInfo, andAppstate: application.applicationState)
            
        }
        
        print("NOTIFICATION = \(userInfo)")
        
    }

    func applicationDidBecomeActive(application: UIApplication) {
        
        let unreadCount = Hotline.sharedInstance().unreadCount()
        UIApplication.sharedApplication().applicationIconBadgeNumber = unreadCount;
        
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        
        if url.host == nil {
            
            return true
            
        }
        
        let query = url.queryDictionary()
        
        if url.path != nil, let path = url.path {
            
            if path.characters.count > 0 {
                
                let path = path.substringFromIndex(path.startIndex.advancedBy(1))
                
                return self.handleUrl(app, entity: url.host!, path: path.componentsSeparatedByString("/"), query: query)
                
            } else {
                
                return self.handleUrl(app, entity: url.host!, query: query)
                
            }
            
        }
        
        return true
  
    }

    func handleUrl(application:UIApplication, entity:String, path:[String] = [], query:Parameters? = nil) -> Bool {
        
        application.startURL = StartURL(entity: entity, path: path, query: query)
        
        return true
        
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        Branch.getInstance().handleDeepLink(url)
        
        return true
    }

    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        
        return Branch.getInstance().continueUserActivity(userActivity)

    }

    
}

